package json.transformer;

import json.transformer.service.AdiType;
import json.transformer.service.ConvertResourceService;
import net.sf.saxon.s9api.SaxonApiException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.test.context.junit4.SpringRunner;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@EnableAutoConfiguration
public class CheckConvertJsonToXml {

    @Autowired
    private ConvertResourceService convertResourceService;
    private final String SUCCESS_JSON_PATH = "src/test/resources/success_json.json";
    private final String CORRUPTED_JSON_PATH = "src/test/resources/corrupted_id.json";
    private final String TITLE_JSON_PATH = "src/test/resources/without_title_json.json";

    private final String FILE_CABLE_LENS_PATH = "src/test/resources/cable_lens.xsd";
    private final String FILE_CISCO_PATH = "src/test/resources/cisco.xsd";

    private final String ID_FIRST_MOVIE = "3380768";
    private final String ID_SECOND_MOVIE = "5585439";

    @Test
    public void testConvert() throws IOException, SaxonApiException {
        Path path = Paths.get(SUCCESS_JSON_PATH);
        String json = Files.readString(path);

        convertResourceService.convertJsonToXml(json, AdiType.CABLE_LENS);

        Resource resource = convertResourceService.getXmlMovieById(ID_FIRST_MOVIE,  AdiType.CABLE_LENS);

        String resultValidate = validateXml(resource.getFile(), AdiType.CABLE_LENS);

        Assert.assertEquals("ok", resultValidate);
    }

    @Test
    public void testConvertException() throws IOException, SaxonApiException {
        Path path = Paths.get(CORRUPTED_JSON_PATH);
        String json = Files.readString(path);

        List<String> resultConvert = convertResourceService.convertJsonToXml(json, AdiType.CABLE_LENS);

        Assert.assertEquals(1
                , resultConvert.size());
    }

    @Test
    public void testValidateException() throws IOException, SaxonApiException {
        Path path = Paths.get(TITLE_JSON_PATH);
        String json = Files.readString(path);

        convertResourceService.convertJsonToXml(json, AdiType.CABLE_LENS);

        Resource resource = convertResourceService.getXmlMovieById(ID_FIRST_MOVIE,  AdiType.CABLE_LENS);

        String resultValidate = validateXml(resource.getFile(), AdiType.CABLE_LENS);

        Assert.assertNotEquals("ok", resultValidate);
    }

    @Test
    public void testConvertForDifferentAdiType() throws IOException, SaxonApiException {
        Path path = Paths.get(SUCCESS_JSON_PATH);

        String json = Files.readString(path);

        convertResourceService.convertJsonToXml(json, AdiType.CABLE_LENS);
        convertResourceService.convertJsonToXml(json, AdiType.CISCO);

        Resource resourceCableLens = convertResourceService.getXmlMovieById(ID_FIRST_MOVIE,  AdiType.CABLE_LENS);
        Resource resourceCisco = convertResourceService.getXmlMovieById(ID_FIRST_MOVIE,  AdiType.CISCO);

        String cableLensValidate = validateXml(resourceCableLens.getFile(), AdiType.CABLE_LENS);
        String ciscoValidate = validateXml(resourceCisco.getFile(), AdiType.CABLE_LENS);

        Assert.assertEquals("ok", cableLensValidate);
        Assert.assertNotEquals("ok", ciscoValidate);
    }

    @Test
    public void testXsd() throws IOException, SaxonApiException {
        Path path = Paths.get(SUCCESS_JSON_PATH);

        String json = Files.readString(path);

        convertResourceService.convertJsonToXml(json, AdiType.CABLE_LENS);

        Resource resourceFirstCisco = convertResourceService.getXmlMovieById(ID_FIRST_MOVIE,  AdiType.CABLE_LENS);
        Resource resourceSecondCisco = convertResourceService.getXmlMovieById(ID_SECOND_MOVIE,  AdiType.CABLE_LENS);

        String ciscoFirstValidate = validateXml(resourceFirstCisco.getFile(), AdiType.CABLE_LENS);
        String ciscoSecondValidate = validateXml(resourceSecondCisco.getFile(), AdiType.CABLE_LENS);

        System.out.println(ciscoFirstValidate);
        System.out.println(ciscoSecondValidate);

        Assert.assertEquals("ok", ciscoFirstValidate);
        Assert.assertNotEquals("ok", ciscoSecondValidate);
    }

    private String validateXml(File resource, AdiType adiType) {
        String schemaPath;
        if (adiType == AdiType.CABLE_LENS) {
            schemaPath = FILE_CABLE_LENS_PATH;
        } else {
            schemaPath = FILE_CISCO_PATH;
        }

        File schemaFile = new File(schemaPath);
        Source xmlFile = new StreamSource(resource);
        SchemaFactory schemaFactory = SchemaFactory
                .newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        try {
            Schema schema = schemaFactory.newSchema(schemaFile);
            Validator validator = schema.newValidator();
            validator.validate(xmlFile);
            System.out.println(xmlFile.getSystemId() + " is valid");
            return "ok";
        } catch (SAXException | IOException e) {
            System.out.println(xmlFile.getSystemId() + " is NOT valid reason:" + e);
            return e.getMessage();
        }
    }

}
