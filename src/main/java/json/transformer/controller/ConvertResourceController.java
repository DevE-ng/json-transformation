package json.transformer.controller;

import json.transformer.service.AdiType;
import json.transformer.service.ConvertResourceService;
import net.sf.saxon.s9api.SaxonApiException;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/convert")
public class ConvertResourceController {
    private final ConvertResourceService convertResourceService;

    public ConvertResourceController(ConvertResourceService convertResourceService) {
        this.convertResourceService = convertResourceService;
    }

    @PostMapping
    public ResponseEntity<List<String>> addMovies(HttpServletRequest request, @RequestParam AdiType adiType) throws IOException, SaxonApiException {
        String req = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
        List<String> result = convertResourceService.convertJsonToXml(req, adiType);
        return ResponseEntity.ok(result);
    }
}



