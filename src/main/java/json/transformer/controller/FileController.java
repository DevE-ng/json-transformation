package json.transformer.controller;

import json.transformer.service.AdiType;
import json.transformer.service.ConvertResourceService;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.MalformedURLException;

@RestController
@RequestMapping("/file")
public class FileController {

    private final ConvertResourceService convertResourceService;

    public FileController(ConvertResourceService convertResourceService) {
        this.convertResourceService = convertResourceService;
    }


    @GetMapping("/{idMovie}")
    public ResponseEntity<Resource> getMovie(@PathVariable String idMovie, @RequestParam AdiType adiType) throws MalformedURLException {
        Resource file = convertResourceService.getXmlMovieById(idMovie, adiType);
        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=\"" + file.getFilename() + "\"")
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .body(file);

    }

}
