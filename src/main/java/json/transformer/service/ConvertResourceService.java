package json.transformer.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import json.transformer.config.FolderPropConfig;
import json.transformer.exception.InvalidJsonException;
import json.transformer.service.util.FileUtil;
import net.sf.saxon.s9api.*;
import net.sf.saxon.trace.XSLTTraceListener;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Service
public class ConvertResourceService {

    private final FolderPropConfig folderPropConfig;
    private final FileUtil fileUtil;
    private final ObjectMapper objectMapper = new ObjectMapper();

    public ConvertResourceService(FolderPropConfig folderPropConfig, FileUtil fileUtil) {
        this.folderPropConfig = folderPropConfig;
        this.fileUtil = fileUtil;
    }

    public List<String> convertJsonToXml(String json, AdiType adiType) throws SaxonApiException, IOException {
        if (isJSONValid(json)) {
            DOMSource xmlWrapper = appendJsonToXml(json);
            Processor processor = new Processor(false);
            XsltCompiler compiler = processor.newXsltCompiler();

            XsltExecutable stylesheet = compiler.compile(new StreamSource(
                    new File( folderPropConfig.getTemplate() + adiType +"/"+"template.xslt")));

            Xslt30Transformer transformer = stylesheet.load30();
            transformer.setTraceListener(new XSLTTraceListener());
            transformer.setBaseOutputURI("file:///"+fileUtil.toURIfile(folderPropConfig.getTemp()));
            try {
                transformer.transform(xmlWrapper, new NullDestination());
            } catch (SaxonApiException ex) {
                return Collections.singletonList("Your id attributes in json file are invalid or missing");
            }

            List<String> result = fileUtil.getFileNamesFromDirectory(folderPropConfig.getTemp());
            fileUtil.moveFile(folderPropConfig.getTemp(), folderPropConfig.getBase()+adiType+'/');

            return result;
        }
        throw new InvalidJsonException("Your json is incorrect");
    }

    public Resource getXmlMovieById(String id, AdiType adiType) throws MalformedURLException {
        File dir = new File(folderPropConfig.getBase()+"/"+adiType);
        File[] arrFiles = dir.listFiles();
        File fileResult = Arrays.stream(arrFiles)
                .filter((file -> file.getName().contains(id)))
                .findFirst().orElseThrow();

        Path path = Paths.get(fileResult.getPath());
        Resource resource = new UrlResource(path.toUri());

        if (resource.exists() || resource.isReadable()) {
            return resource;
        }
        return null;
    }

    private DOMSource appendJsonToXml(String json) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        try {
            builder = factory.newDocumentBuilder();
            Document doc = builder.newDocument();

            Element root = doc.createElementNS("", "data");
            root.appendChild(doc.createTextNode(json));
            doc.appendChild(root);

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            DOMSource source = new DOMSource(doc);

            StreamResult console = new StreamResult(System.out);

            transformer.transform(source, console);

            return source;
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return null;
    }

    private boolean isJSONValid(String json) throws IOException {
        boolean valid = true;
        try{
            objectMapper.readTree(json);
        } catch(JsonProcessingException e){
            valid = false;
        }
        return valid;
    }
}
