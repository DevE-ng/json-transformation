package json.transformer.service.util;

import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
public class FileUtil {
    public List<String> getFileNamesFromDirectory(String path) {
        File dir = new File(path);
        File[] arrFiles = dir.listFiles();
        Pattern pattern = Pattern.compile("movie-(.+?).xml");
        return Arrays.stream(arrFiles)
                .map((file) -> {
                    String fileName = file.getName();
                    Matcher matcher = pattern.matcher(fileName);
                    if (matcher.find() && matcher.groupCount() > 1) {
                        return matcher.group(1);
                    }
                    return file.getName();
                })
                .collect(Collectors.toList());
    }

    public void moveFile(String dirFrom, String dirTo) throws IOException {
        createDirectory(dirTo);
        for (File myFile : new File(dirFrom).listFiles())
            Files.move(myFile.toPath(), Paths.get(dirTo + myFile.getName()), StandardCopyOption.REPLACE_EXISTING);
    }

    public void createDirectory(String directory) throws IOException {
        Path path = Paths.get(directory);
        if (!Files.exists(path)) {
            Files.createDirectory(path);
        }
    }

    public String toURIfile(String filePath){
        return filePath.replace("\\", "/");
    }
}
