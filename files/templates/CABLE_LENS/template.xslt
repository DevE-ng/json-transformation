<?xml version="1.0"?>
<xsl:stylesheet
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
        xmlns:math="http://www.w3.org/2005/xpath-functions/math"
        xmlns:xs="http://www.w3.org/2001/XMLSchema"
        xmlns:fn="http://www.w3.org/2005/xpath-functions"
        exclude-result-prefixes="xs math" version="3.0">
    <xsl:output indent="yes" omit-xml-declaration="yes"/>

    <xsl:template match="data">
        <!-- create a new root tag -->

        <xsl:apply-templates select="json-to-xml(.)"/>
        <!--                  <xsl:copy-of select="json-to-xml(.)"/>-->

    </xsl:template>


    <xsl:template match="//array[@key='items']" xpath-default-namespace="http://www.w3.org/2005/xpath-functions">
        <xsl:for-each select="map">
            <xsl:variable name="file-name" select="concat('movie-',number[@key='content_id'],'.xml')"/>
            <xsl:variable name="description" select="string[@key='description_en']"/>
            <xsl:variable name="title" select="string[@key='title_en']"/>
            <xsl:variable name="id" select="number[@key='content_id']"/>
            <xsl:if test="number[@key='content_id'] != ''">
                <xsl:result-document method="xml" href="{$file-name}">
                    <ADI>
                        <Metadata>
                            <AMS Description="" Title="{$title}" ID="{$id}">
                            </AMS>
                        </Metadata>

                        <xsl:variable name="actors"
                                      select="string-join(array[@key='actors']/map/string[@key='name_en'], ';')"/>


                        <App_Data App="MOD" Name="Actors" Value="{$actors}"/>
                    </ADI>
                </xsl:result-document>
            </xsl:if>
        </xsl:for-each>

    </xsl:template>


</xsl:stylesheet>
